import {Dimensions, LayoutRectangle} from 'react-native';

const window = Dimensions.get('window');

// Width smaller than the widthBoundary is considered phones.
// ISSUE? - Kinna, is this required?
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const widthBoundary = 576;
const totalPixels = window.width + window.height;
const totalMobileTabletBoundary = 1500;

enum ScreenCategory {
  SMALL,
  MEDIUM,
  LARGE,
}

const SMALL_MAX_SIZE = 480;
const MEDIUM_MAX_SIZE = 768;

const getScreenCategory = () => {
  const size = Math.min(window.width, window.height);
  if (size < SMALL_MAX_SIZE) {
    return ScreenCategory.SMALL;
  } else if (size < MEDIUM_MAX_SIZE) {
    return ScreenCategory.MEDIUM;
  } else {
    return ScreenCategory.LARGE;
  }
};

const getPointSize = () => {
  switch (getScreenCategory()) {
    case ScreenCategory.SMALL:
      return 12;
    case ScreenCategory.MEDIUM:
      return 14;
    case ScreenCategory.LARGE:
      return 16;
    default:
      return 0;
  }
};

export const Window = {
  width: Math.min(window.width, window.height),
  height: Math.max(window.width, window.height),
  horizontal: window.width > window.height,
  screenCategory: getScreenCategory(),
};
export const Color = {
  darkInk: '#313047',
  darkTeal: '#004D40',
  corbeau: '#13101F',
  indigo: '#1A237E',
  fadeGreen: '#81C784',
  fadeTeal: '#80CBC4',
  mistLightBlue: '#E1F5FE',
  mistGrey: '#ECEFF1',
  mistLightGrey: '#FAFAFA',
  mistGreen: '#E8F5E9',
  pLaurelGreen: '#A4B494',
  p11Grey: '#BEC5AD',
  pArsenic: '#3B5249',
  pWinterGreen: '#519872',
  pJetPurp: '#34252F',
  pRaisinBlack: '#232420',
  pDarkJungle: '#1B2426',
  vanGoghGreen: '#69C992',
  midoriGreen: '#3EB769',
  warmSun: '#FCC62B',
  aqua: '#24a5db',
};

export const Size = {
  consistentPoint: (Window.width + Window.height) * 0.01,
  point: getPointSize(),
  edge: getPointSize() * 2,
  // Categorising the device to different sizes.
  categoryWidthPoint:
    totalPixels < 1200 ? 10 : totalPixels < totalMobileTabletBoundary ? 12 : 20,
  categoryHeightPoint:
    totalPixels < 1200 ? 10 : totalPixels < totalMobileTabletBoundary ? 12 : 15,
};

export const FontSize = {
  sessionHeaderSmall: Size.point,
  small: Size.point * 1.2,
  default: Size.categoryHeightPoint * 1.5,
  large: Size.point * 2,
  screenTitle: Size.point * 2.5,
};

export const IconSize = {
  // small: Window.width * 0.045,
  small: Size.categoryHeightPoint * 1.5,
  setup: Window.width * 0.075,
  tabBar: Size.point * 1.8,
  medium: Size.categoryHeightPoint * 3,
  large: Window.width * 0.15,
};

export const ComponentSize = {
  sessionHeader: Size.point * 5,
  sessionControls: Size.point * 6,
};

export const SessionHeaderHeight = Size.point * 5;
export const SessionControlsBarHeight = Size.point * 6;

const isWiderThan = (layout: LayoutRectangle, ratio: number[]) => {
  return layout.width / layout.height > ratio[0] / ratio[1];
};

const isTallerThan = (layout: LayoutRectangle, ratio: number[]) => {
  return !isWiderThan(layout, ratio);
};

export const AspectRatio = {
  /**
   * Whether the layout is wider than the given aspect ratio.
   * @param layout The layout for the view
   * @param ratio Array with 2 number representing the width/height ratio
   */
  widerThan: isWiderThan,
  /**
   * Whether the layout is taller than the given aspect ratio.
   * @param layout The layout for the view
   * @param ratio Array with 2 number representing the width/height ratio
   */
  tallerThan: isTallerThan,
};
