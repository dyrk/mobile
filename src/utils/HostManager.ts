/**
 * A singleton class that is responsible for managing the Dyrk host connection.
 */
export class HostManager {
  /**
   * Access to the singleton object instance across the whole app.
   * @returns the static singleton instance.
   */
  public static getInstance(): HostManager {
    if (!HostManager.instance) {
      HostManager.instance = new HostManager();
    }
    return HostManager.instance;
  }

  /**
   * The static instance of the class.
   */
  private static instance: HostManager;
  /**
   * IP address of the host device to connect to.
   */
  private hostIP: string;

  private constructor() {
    this.hostIP = '127.0.0.1'; // by default set to local host
  }

  public getHostIP = () => {
    return this.hostIP;
  };
  /**
   *
   * @returns the host ip prepended with http info
   */
  public getFormattedHostIP = () => {
    return 'http://' + this.hostIP;
  };

  public setHostIP = (newIP: string) => {
    this.hostIP = newIP;
  };
}
