import {Interval, OutputEvent} from '../Types';

const MAX_REPEATS = 20;
const secsInSecs = 1;
const secsInMins = secsInSecs * 60;
const secsInHours = secsInMins * 60;
const secsInDays = secsInHours * 24;
const secsInWeeks = secsInDays * 7;
const secsInMonths = secsInDays * 30; // <---- What is the agreed upon definition of a month?

export const createRepeatedEvents = (
  event: OutputEvent,
  interval: Interval,
) => {
  switch (interval) {
    case 'none':
      return [event];
    case 'second':
      let secondEvents: OutputEvent[] = [];
      for (let i = 0; i < MAX_REPEATS; i++) {
        secondEvents.push({...event, relative_timestamp: i * secsInSecs});
      }
      return secondEvents;
    case 'minute':
      let minuteEvents: OutputEvent[] = [];
      for (let i = 0; i < MAX_REPEATS; i++) {
        minuteEvents.push({...event, relative_timestamp: i * secsInMins});
      }
      return minuteEvents;
    case 'hour':
      let hourEvents: OutputEvent[] = [];
      for (let i = 0; i < MAX_REPEATS; i++) {
        hourEvents.push({...event, relative_timestamp: i * secsInHours});
      }
      return hourEvents;
    case 'day':
      let dayEvents: OutputEvent[] = [];
      for (let i = 0; i < MAX_REPEATS; i++) {
        dayEvents.push({...event, relative_timestamp: i * secsInDays});
      }
      return dayEvents;
    case 'week':
      let weekEvents: OutputEvent[] = [];
      for (let i = 0; i < MAX_REPEATS; i++) {
        weekEvents.push({...event, relative_timestamp: i * secsInWeeks});
      }
      return weekEvents;
    case 'month':
      let monthEvents: OutputEvent[] = [];
      for (let i = 0; i < MAX_REPEATS; i++) {
        monthEvents.push({...event, relative_timestamp: i * secsInMonths});
      }
      return monthEvents;

    default:
      return [event];
  }
};
