export const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};

/**
 * Helper function to force a block of code to sleep for a given interval (ms) if the function is called with the await keyword.
 * The promise just returns a string detailing the number of ms slept.
 * @param ms The desired number of ms to sleep for.
 */
export const sleep = async (ms: number) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('System just Slept for - ' + ms + ' miliseconds');
    }, ms);
  });
};
