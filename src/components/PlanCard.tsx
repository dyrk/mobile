import {FlatList, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Button} from 'react-native-elements';
import {Icon} from 'react-native-elements/dist/icons/Icon';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withDelay,
  withSpring,
  withTiming,
} from 'react-native-reanimated';
import {OutputEvent, SchedulePlan} from '../Types';
import {Color, IconSize, Size} from '../utils/CommonStyles';
import {PlanTaskRow} from './PlanTaskRow';
import {sleep, uuidv4} from '../utils/Utilities';

const closedCardHeight = Size.consistentPoint * 12.5;
const openCardHeight = Size.consistentPoint * 37;

type Props = {
  planInfo: SchedulePlan;
  onPlanConfirmed: (planToSend: SchedulePlan) => {};
};

const dummyTaskRow: OutputEvent = {
  event_type: 'output_event',
  relative_timestamp: 0,
  output_amount: [],
  output_units: [],
};
export const PlanCard: React.FC<Props> = props => {
  const [isCardOpen, setIsCardOpen] = useState(false);
  const [addedTasks, setAddedTasks] = useState<OutputEvent[]>([]);
  const [displayRows, setDisplayRows] = useState<OutputEvent[]>([dummyTaskRow]);
  const [taskRows, setTaskRows] = useState(addedTasks.length + 1);
  // creating shared value via useSharedValue
  const boxHeight = useSharedValue(closedCardHeight);
  const chevronRotation = useSharedValue('0deg');
  const innerOpacity = useSharedValue(0);
  // creating worklet via useAnimatedStyle, and incorporating the withTiming method
  const boxAnimation = useAnimatedStyle(() => {
    'worklet'; // <-- This shouldn't be needed, but the Reanimated babel plugin doesn't seem to work so it is.
    return {
      // height: withTiming(boxHeight.value, {duration: 500}),
      height: withSpring(boxHeight.value, {damping: 14}),
    };
  });
  const chevronAnimation = useAnimatedStyle(() => {
    'worklet'; // <-- This shouldn't be needed, but the Reanimated babel plugin doesn't seem to work so it is.
    return {
      // height: withTiming(boxHeight.value, {duration: 500}),
      transform: [{rotate: withSpring(chevronRotation.value, {damping: 14})}],
    };
  });
  const innerAnimation = useAnimatedStyle(() => {
    'worklet'; // <-- This shouldn't be needed, but the Reanimated babel plugin doesn't seem to work so it is.
    return {
      // height: withTiming(boxHeight.value, {duration: 500}),
      opacity: isCardOpen
        ? withTiming(innerOpacity.value, {duration: 100})
        : withDelay(200, withTiming(innerOpacity.value, {duration: 200})),
    };
  });
  // function that toggles the value of boxHeight so it can expand and contract
  const toggleOpen = async () => {
    if (isCardOpen) {
      handleCloseCard();
    } else {
      handleOpenCard();
    }
  };

  const handleOpenCard = async () => {
    boxHeight.value = openCardHeight;
    innerOpacity.value = 1;
    chevronRotation.value = '180deg';
    await sleep(200);
    setIsCardOpen(true);
  };
  const handleCloseCard = async () => {
    setAddedTasks([]);
    setDisplayRows([dummyTaskRow]);
    setTaskRows(1);
    innerOpacity.value = 0;
    await sleep(100);
    setIsCardOpen(false);
    boxHeight.value = closedCardHeight;
    innerOpacity.value = 1;
    chevronRotation.value = '0deg';
  };

  const handleAddTask = (tasksToAdd: OutputEvent[]) => {
    const addedTasksArray = addedTasks;
    tasksToAdd.forEach(task => {
      addedTasksArray.push(task);
    });
    setAddedTasks(addedTasksArray);
    const currentDisplayRows = displayRows;
    currentDisplayRows.push(dummyTaskRow);
    setDisplayRows(currentDisplayRows);
    setTaskRows(addedTasksArray.length + 1);
    console.debug('Added Tasks!!! - ', addedTasksArray);
  };

  const handleConfirmPlan = () => {
    props.onPlanConfirmed({
      name: 'Plan From App',
      plan_id: uuidv4(),
      object_type: 'schedule_plan',
      comment: 'This plan came from the app!',
      events: addedTasks,
    });
  };

  const renderItem = ({item}: {item: OutputEvent}) => {
    // const backgroundColor = item.id === selectedId ? '#6e3b6e' : '#f9c2ff';
    // const color = item.id === selectedId ? 'white' : 'black';

    return <PlanTaskRow onTaskSubmit={handleAddTask} />;
  };

  return (
    /* Animated.View component, with the typical styles contained in styles.box,
        and the worklet "boxHeight" that updates the height property alongside it */
    <Animated.View style={[styles.box, boxAnimation]}>
      {/* button that fires off toggleHeight() function every time it's pressed */}
      <Button
        title={props.planInfo.name}
        titleStyle={{color: Color.darkTeal, fontSize: 20}}
        TouchableComponent={TouchableWithoutFeedback}
        onPress={() => toggleOpen()}
        type={'clear'}
        icon={
          <Animated.View style={[{marginHorizontal: 5}, chevronAnimation]}>
            <Icon
              name={'chevron-down'}
              type={'entypo'}
              iconStyle={{opacity: 0.4}}
            />
          </Animated.View>
        }
        iconRight={true}
      />
      {isCardOpen && (
        <Animated.View style={[styles.innerBox, innerAnimation]}>
          <FlatList
            data={displayRows}
            style={styles.taskRowsContainer}
            renderItem={renderItem}
            keyExtractor={(item: OutputEvent, index: number) =>
              item.event_type + index
            }
            extraData={displayRows}
          />
        </Animated.View>
      )}
      {addedTasks.length > 0 && (
        <Button
          title={'Confirm Plan'}
          onPress={handleConfirmPlan}
          buttonStyle={{
            backgroundColor: Color.darkInk,
            borderRadius: Size.edge,
          }}
        />
      )}
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: '90%',
    height: closedCardHeight,
    backgroundColor: Color.mistLightBlue,
    borderRadius: 15,
    margin: 10,
    padding: 20,
    // display: 'flex',
    elevation: 5,
  },
  innerBox: {
    height: Size.consistentPoint * 25,
    // backgroundColor: Color.pLaurelGreen,
    justifyContent: 'center',
    alignItems: 'center',
  },
  planTypeIconContainer: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  iconContainer: {
    backgroundColor: Color.mistLightGrey,
    elevation: 5,
    padding: 10,
    borderRadius: Size.edge * 2,
    height: Size.consistentPoint * 6,
    width: Size.consistentPoint * 6,
    alignItems: 'center',
    justifyContent: 'center',
    // height:
  },
  taskRowsContainer: {
    height: '100%',
    width: '100%',
    paddingTop: Size.consistentPoint * 2,
  },
});
