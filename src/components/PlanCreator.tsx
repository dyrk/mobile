import {FlatList, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Button} from 'react-native-elements';
import {OutputEvent, SchedulePlan} from '../Types';
import {Color, IconSize, Size} from '../utils/CommonStyles';
import {PlanTaskRow} from './PlanTaskRow';
import {uuidv4} from '../utils/Utilities';

type Props = {
  planInfo: SchedulePlan;
  onPlanConfirmed: (planToSend: SchedulePlan) => {};
};

const dummyTaskRow: OutputEvent = {
  event_type: 'output_event',
  relative_timestamp: 0,
  output_amount: [],
  output_units: [],
};
export const PlanCreator: React.FC<Props> = props => {
  const [addedTasks, setAddedTasks] = useState<OutputEvent[]>([]);
  const [displayRows, setDisplayRows] = useState<OutputEvent[]>([dummyTaskRow]);
  const [taskRows, setTaskRows] = useState(addedTasks.length + 1);
  // creating shared value via useSharedValue
  // function that toggles the value of boxHeight so it can expand and contract

  const handleAddTask = (tasksToAdd: OutputEvent[]) => {
    const addedTasksArray = addedTasks;
    tasksToAdd.forEach(task => {
      addedTasksArray.push(task);
    });
    setAddedTasks(addedTasksArray);
    const currentDisplayRows = displayRows;
    currentDisplayRows.push(dummyTaskRow);
    setDisplayRows(currentDisplayRows);
    setTaskRows(addedTasksArray.length + 1);
    console.debug('Added Tasks!!! - ', addedTasksArray);
  };

  const handleConfirmPlan = () => {
    props.onPlanConfirmed({
      name: 'Plan From App',
      plan_id: uuidv4(),
      object_type: 'schedule_plan',
      comment: 'This plan came from the app!',
      events: addedTasks,
    });
  };

  const renderItem = ({item}: {item: OutputEvent}) => {
    // const backgroundColor = item.id === selectedId ? '#6e3b6e' : '#f9c2ff';
    // const color = item.id === selectedId ? 'white' : 'black';

    return <PlanTaskRow onTaskSubmit={handleAddTask} />;
  };

  return (
    /* Animated.View component, with the typical styles contained in styles.box,
        and the worklet "boxHeight" that updates the height property alongside it */
    <View style={[styles.box]}>
      {/* button that fires off toggleHeight() function every time it's pressed */}
      <FlatList
        data={displayRows}
        style={styles.taskRowsContainer}
        renderItem={renderItem}
        keyExtractor={(item: OutputEvent, index: number) =>
          item.event_type + index
        }
        extraData={displayRows}
      />
      <View style={styles.buttonContainer}>
        {addedTasks.length > 0 && (
          <Button
            title={'Confirm Plan'}
            onPress={handleConfirmPlan}
            buttonStyle={{
              backgroundColor: Color.darkInk,
              borderRadius: Size.edge,
            }}
          />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    width: '90%',
    height: '100%',
    backgroundColor: Color.mistLightBlue,
    borderRadius: 15,
    margin: 10,
    padding: 20,
    // display: 'flex',
    // elevation: 5,
  },
  buttonContainer: {
    height: '10%',
    // backgroundColor: 'green',
    justifyContent: 'center',
  },
  innerBox: {
    height: Size.consistentPoint * 25,
    // backgroundColor: Color.pLaurelGreen,
    justifyContent: 'center',
    alignItems: 'center',
  },
  planTypeIconContainer: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  iconContainer: {
    backgroundColor: Color.mistLightGrey,
    elevation: 5,
    padding: 10,
    borderRadius: Size.edge * 2,
    height: Size.consistentPoint * 6,
    width: Size.consistentPoint * 6,
    alignItems: 'center',
    justifyContent: 'center',
    // height:
  },
  taskRowsContainer: {
    height: '80%',
    width: '100%',
    // backgroundColor: 'red',

    paddingVertical: Size.consistentPoint,
  },
});
