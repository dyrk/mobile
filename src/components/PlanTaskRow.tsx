import React, {useRef, useState} from 'react';
import {
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  LayoutAnimation,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Button} from 'react-native-elements';
import {Icon} from 'react-native-elements/dist/icons/Icon';
import {Input} from 'react-native-elements/dist/input/Input';
import PagerView from 'react-native-pager-view';
import {
  allIntervals,
  Interval,
  OutputEvent,
  TaskType,
  TaskTypeName,
  TaskTypeObject,
  TaskTypes,
} from '../Types';
import {Color, FontSize, IconSize, Size} from '../utils/CommonStyles';
import {Picker} from '@react-native-picker/picker';
import {sleep} from '../utils/Utilities';
import {createRepeatedEvents} from '../utils/PlanHelper';

type Props = {
  onTaskSubmit: (tasksToAdd: OutputEvent[]) => void;
};
export const PlanTaskRow: React.FC<Props> = props => {
  const pagerViewRef = useRef<PagerView | null>(null);
  const [selectedInterval, setSelectedInterval] = useState<Interval>('none');
  const [selectedAmount, setSelectedAmount] = useState(0);
  const [hasTaskTypeBeenSelected, setHasTaskTypeBeenSelected] = useState(false);
  const [hasTaskBeenSubmitted, setHasTaskBeenSubmitted] = useState(false);
  const [submittedAmountString, setSubmittedAmountString] = useState('');
  const [submittedIntervalString, setSubmittedIntervalString] = useState('');
  const [selectedTaskType, setSelectedTaskType] = useState(
    null as null | TaskType,
  );

  const handleTaskSubmit = () => {
    // console.debug(
    //   'Task submit with type - ',
    //   tasktype,
    //   'with amount - ',
    //   amount,
    // );
    switch (selectedTaskType) {
      case TaskType.WATER:
        setSubmittedAmountString('Add ' + selectedAmount + ' ml of water');
        setSubmittedIntervalString('Every ' + selectedInterval);
        const singleWaterEvent: OutputEvent = {
          event_type: 'output_event',
          relative_timestamp: 0,
          output_amount: [selectedAmount, 0, 0, 0],
          output_units: ['ml', 'ml', 'ml', 'ml'],
        };
        props.onTaskSubmit(
          createRepeatedEvents(singleWaterEvent, selectedInterval),
        );

        break;
      case TaskType.LIGHT:
        setSubmittedAmountString('Add ' + selectedAmount + ' lux of light');
        setSubmittedIntervalString('Every ' + selectedInterval);
        const singleLightEvent: OutputEvent = {
          event_type: 'output_event',
          relative_timestamp: 0,
          output_amount: [selectedAmount, 0, 0, 0],
          output_units: ['lx', 'lx', 'lx', 'lx'],
        };
        props.onTaskSubmit(
          createRepeatedEvents(singleLightEvent, selectedInterval),
        );
        break;
      default:
        setSubmittedAmountString('Unknown task added');
        break;
    }
    setHasTaskBeenSubmitted(true);
    pagerViewRef.current?.setPage(4);
  };

  const selectTaskType = (taskType: TaskType) => {
    // console.log('Selected Task Type = ', taskType);
    setSelectedTaskType(taskType);
    setHasTaskTypeBeenSelected(true);
    pagerViewRef.current?.setPage(2);
    // setTaskIcons([]);
  };

  const selectAmount = async (amount: string) => {
    setSelectedAmount(parseInt(amount, 10));
    // await sleep(500);
    pagerViewRef.current?.setPage(3);
  };

  const handlePagerGoBack = (currentPageNumber: number) => {
    const currentPageCorrected = currentPageNumber - 1;
    pagerViewRef.current?.setPage(currentPageCorrected - 1);
  };

  const renderEntryScene = () => {
    return (
      <View key="1" style={[{opacity: 0.7}]}>
        <TouchableOpacity
          style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
          onPress={() => pagerViewRef.current?.setPage(1)}>
          <Text style={{fontSize: FontSize.large * 0.8}}> + New Event </Text>
        </TouchableOpacity>
      </View>
    );
  };

  const renderEventTypeScene = () => {
    return (
      <View key="2" style={styles.planTypeViewContainer}>
        <Icon
          name={'water-outline'}
          type={'ionicon'}
          color={Color.aqua}
          size={IconSize.large * 0.6}
          containerStyle={styles.iconContainer}
          onPress={() => selectTaskType(TaskType.WATER)}
        />
        <Icon
          name={'lightbulb'}
          type={'font-awesome-5'}
          color={Color.warmSun}
          size={IconSize.large * 0.55}
          containerStyle={styles.iconContainer}
          onPress={() => selectTaskType(TaskType.LIGHT)}
        />
      </View>
    );
  };

  const renderAttributesScene = () => {
    switch (selectedTaskType) {
      case TaskType.WATER:
        return (
          <View
            key="3"
            style={[
              styles.planAttributesViewContainer,
              {flexDirection: 'row'},
              // {backgroundColor: Color.aqua},
            ]}>
            <Icon
              name={'chevron-left'}
              type={'entypo'}
              iconStyle={{opacity: 0.5}}
              onPress={() => handlePagerGoBack(3)}
            />
            <View style={{flex: 1}}>
              <Text style={styles.amountSelectionText}>
                Select Amount of Water in Ml
              </Text>
              <Input
                inputStyle={{textAlign: 'center'}}
                keyboardType={'number-pad'}
                onSubmitEditing={event => {
                  selectAmount(event.nativeEvent.text);
                }}
              />
            </View>
          </View>
        );
      case TaskType.LIGHT:
        return (
          <View
            key="3"
            style={[
              styles.planAttributesViewContainer,
              {flexDirection: 'row'},
              // {backgroundColor: Color.warmSun},
            ]}>
            <Icon
              name={'chevron-left'}
              type={'entypo'}
              iconStyle={{opacity: 0.5}}
              onPress={() => handlePagerGoBack(3)}
            />
            <View style={{flex: 1}}>
              <Text style={styles.amountSelectionText}>
                Select Amount of Light in Lux
              </Text>
              <Input
                inputStyle={{textAlign: 'center'}}
                keyboardType={'number-pad'}
                onSubmitEditing={event => {
                  selectAmount(event.nativeEvent.text);
                }}
              />
            </View>
          </View>
        );
      default:
        return <View />;
    }
  };

  const renderTimeInterval = () => {
    return (
      <View
        key="4"
        style={[
          styles.planAttributesViewContainer,
          {flexDirection: 'row'},
          // {backgroundColor: Color.warmSun},
        ]}>
        <Icon
          name={'chevron-left'}
          type={'entypo'}
          iconStyle={{opacity: 0.5}}
          onPress={() => handlePagerGoBack(4)}
        />
        <Text style={styles.timeSelectionText}>{'Repeat'}</Text>
        <Picker
          style={{backgroundColor: 'white', width: '50%'}}
          selectedValue={selectedInterval}
          onValueChange={(itemValue, itemIndex) =>
            setSelectedInterval(itemValue)
          }>
          {allIntervals.map((interval, index) => (
            <Picker.Item key={index} label={interval} value={interval} />
          ))}
        </Picker>
        <Icon
          name={'check'}
          type={'feather'}
          color={Color.midoriGreen}
          // iconStyle={{opacity: 0.5}}
          onPress={() => handleTaskSubmit()}
        />
      </View>
    );
  };

  const renderReviewScene = () => {
    switch (selectedTaskType) {
      case TaskType.WATER:
        return (
          <View
            key="5"
            style={[
              styles.planAttributesViewContainer,
              styles.reviewContainerBorder,
            ]}>
            <Icon
              name={'water-outline'}
              type={'ionicon'}
              color={Color.darkInk}
              size={IconSize.medium}
              // containerStyle={styles.iconContainer}
            />
            <View>
              <Text style={styles.reviewAmountText}>
                {submittedAmountString}
              </Text>
              <Text style={styles.reviewIntervalText}>
                {submittedIntervalString}
              </Text>
            </View>
          </View>
        );
      case TaskType.LIGHT:
        return (
          <View
            key="5"
            style={[
              styles.planAttributesViewContainer,
              styles.reviewContainerBorder,
            ]}>
            <Icon
              name={'lightbulb'}
              type={'font-awesome-5'}
              color={Color.darkInk}
              size={IconSize.medium}
              // containerStyle={styles.iconContainer}
            />
            <View>
              <Text style={styles.reviewAmountText}>
                {submittedAmountString}
              </Text>
              <Text style={styles.reviewIntervalText}>
                {submittedIntervalString}
              </Text>
            </View>
          </View>
        );
      default:
        return <View />;
    }
  };

  return (
    <PagerView
      ref={pagerViewRef}
      style={[styles.pagerView, hasTaskBeenSubmitted && styles.submittedBox]}
      initialPage={0}
      offscreenPageLimit={2}
      onPageSelected={e => {
        console.log(
          'PagerView On Page Selected :: event = ',
          e.nativeEvent.position,
        );
      }}
      scrollEnabled={false}>
      {renderEntryScene()}
      {renderEventTypeScene()}
      {renderAttributesScene()}

      {renderTimeInterval()}
      {renderReviewScene()}
    </PagerView>
  );
};

const styles = StyleSheet.create({
  planTypeViewContainer: {
    flexDirection: 'row',
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  planAttributesViewContainer: {
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  iconContainer: {
    backgroundColor: Color.mistLightGrey,
    elevation: 5,
    padding: 10,
    borderRadius: Size.edge * 2,
    height: Size.consistentPoint * 6,
    width: Size.consistentPoint * 6,
    alignItems: 'center',
    justifyContent: 'center',
    // height:
  },
  pagerView: {
    height: Size.consistentPoint * 8,
    width: '100%',
  },
  submittedBox: {
    borderLeftColor: Color.midoriGreen,
    borderLeftWidth: 2,
    elevation: 2,
  },
  amountSelectionText: {
    color: Color.darkInk,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  timeSelectionText: {
    fontSize: FontSize.large * 0.8,
    color: Color.darkInk,
    // fontWeight: 'bold',
    textAlign: 'center',
  },
  timeSelectionContainer: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    // backgroundColor: 'green',
  },
  reviewAmountText: {
    color: Color.darkInk,
    fontSize: FontSize.large * 0.7,
    fontWeight: 'bold',
    fontStyle: 'italic',
  },
  reviewIntervalText: {
    color: Color.darkInk,
    fontSize: FontSize.large * 0.65,
    fontWeight: 'bold',
    fontStyle: 'italic',
    opacity: 0.8,
  },
  reviewContainerBorder: {
    backgroundColor: Color.mistLightBlue,
    flexDirection: 'row',
    borderLeftWidth: 3,
    borderLeftColor: Color.midoriGreen,
  },
});
