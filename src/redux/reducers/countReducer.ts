import {COUNTER_CHANGE} from '../constants';
import {ReduxAction} from '../redux-types';

const initialState = {
  count: 0,
};
const countReducer = (state = initialState, action: ReduxAction) => {
  switch (action.type) {
    case COUNTER_CHANGE:
      return {
        ...state,
        count: action.payload,
      };
    default:
      return state;
  }
};
export default countReducer;
