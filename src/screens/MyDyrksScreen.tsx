import React, {useCallback, useState, useRef, useEffect} from 'react';
import {View, Text, StyleSheet, Animated} from 'react-native';
import {Color, Size} from '../utils/CommonStyles';
import {Svg, Circle, Path, G} from 'react-native-svg';
import {useFocusEffect} from '@react-navigation/native';

const AnimatedCircle = Animated.createAnimatedComponent(Circle);

const REVENTLOWSGADE_IP = 'http://192.168.1.12:5000';
const VALBY_IP = 'http://192.168.0.27:5000';
const LABITAT_IP = 'http://10.42.3.196:5000';

export const MyDyrksScreen = () => {
  return (
    <View style={styles.HomeContainer}>
      <Text style={styles.text}> Light Sensor </Text>
      {/* <View style={styles.svgArea}>
                <Svg style={{width: "100%", height: "100%"}} viewBox="0 0 100 100">
                    <Circle cx="50" cy="50" r="30" fill="yellow" />
                </Svg>
            </View> */}
      <Svg style={{height: '60%', width: '100%'}} viewBox="0 0 100 100">
        <G id="prefix__Leaf">
          <AnimatedCircle
            cx="50"
            cy="50"
            r="30"
            fill={Color.warmSun}
            origin={'50,50'}
            // scale={scaleAnim.interpolate({
            //     inputRange: [0.5, 50.5],
            //     outputRange: [0.5, 1.5],
            // })}
            scale={lxVal / 70 + 0.5}
          />
        </G>
      </Svg>
    </View>
  );
};

const styles = StyleSheet.create({
  HomeContainer: {
    flex: 1,
    backgroundColor: Color.fadeGreen,
    // padding: Size.edge,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    flex: 1,
    color: Color.mistLightBlue,
    fontSize: 30,
    textAlignVertical: 'center',
  },
  svgArea: {
    flex: 1,
    backgroundColor: Color.darkTeal,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
