import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Color, FontSize, Size} from '../utils/CommonStyles';
import {RouteProp, useNavigation} from '@react-navigation/native';
import LottieView from 'lottie-react-native';
import {
  HomeStackNavigationProp,
  HomeStackParamList,
} from '../navigators/MainStack';
import {Button, Input} from 'react-native-elements';
import {HostManager} from '../utils/HostManager';
import Toast from 'react-native-simple-toast';
import {sleep} from '../utils/Utilities';

type HomeRouteProp = RouteProp<HomeStackParamList, 'Home'>;

export const HomeScreen = () => {
  const [connecting, setConnecting] = useState(false);
  const dummyDyrkName = 'My Dyrk';
  const nav = useNavigation<HomeStackNavigationProp>();

  const openDyrk = () => {
    nav.navigate('HostOverview', {name: dummyDyrkName});
  };

  const setHostIp = (newIP: string) => {
    console.debug('Set host ip - ', newIP);
    const hostManager = HostManager.getInstance();
    hostManager.setHostIP(newIP);
  };

  const tryToConnectToHost = async () => {
    setConnecting(true);
    const hostManager = HostManager.getInstance();
    const hostIPAddress = hostManager.getFormattedHostIP();
    // Toast.show('Clicked Connect');
    console.log('Trying to connect to host with IP - ', hostIPAddress);
    Toast.show('Trying to connect to host with IP - ' + hostIPAddress);
    await sleep(1000);
    try {
      const path = hostIPAddress + '/gethostinfo';
      const result: Response = await fetch(path, {method: 'GET'});
      const text = await result.text();
      // const json = await result.json();
      console.debug('GOT HOST CONFIG INFO RESULT - ', text);
      Toast.show('Connected to ' + hostIPAddress);
      setConnecting(false);
      openDyrk();
    } catch (error) {
      setConnecting(false);
      console.warn("Couldn't get host config info - ", error);
      Toast.show("Couldn't get host config info - " + error);
    }
  };

  return (
    <View style={styles.HomeContainer}>
      {/* <View style={{flex: 1, height: 200, width: 200}}> */}
      <View style={{flex: 1, width: '40%'}}>
        <LottieView
          source={require('../assets/lottieAnims/static-logo.json')}
          // source={require('../assets/lottieAnims/Logo_w_Text.json')}
          autoPlay
          loop
        />
      </View>
      <View style={styles.lowerSection}>
        <View style={styles.ipInputCard}>
          <Input
            defaultValue={'127.0.0.1'}
            style={{}}
            label={'Enter the IP address of your Dyrk Host'}
            labelStyle={{textAlign: 'center'}}
            containerStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainer}
            inputStyle={{textAlign: 'center', fontWeight: 'bold'}}
            keyboardType={'numeric'}
            onChangeText={text => setHostIp(text)}
            onSubmitEditing={e => setHostIp(e.nativeEvent.text)}
          />
        </View>
        <Button
          title={'Connect'}
          buttonStyle={styles.loginButton}
          titleStyle={styles.text}
          loading={connecting}
          onPress={tryToConnectToHost}
          containerStyle={{width: '90%', borderRadius: Size.edge * 2}}
        />
        {/* <TouchableOpacity style={styles.dyrkCard} onPress={openDyrk}>
          <Text style={styles.text}>{dummyDyrkName}</Text>
          <View style={styles.divider} />
          <Text style={{...styles.text, fontSize: 20}}>{targetPath}</Text>
        </TouchableOpacity> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  HomeContainer: {
    flex: 1,
    backgroundColor: Color.mistLightBlue,
    // padding: Size.edge,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    // flex: 1,
    color: Color.midoriGreen,
    fontSize: FontSize.large,
    fontWeight: 'bold',
  },
  lowerSection: {
    flex: 2,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    // backgroundColor: 'red',
  },
  dyrkCard: {
    height: '50%',
    width: '80%',
    backgroundColor: Color.corbeau,
    borderRadius: 40,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginButton: {
    backgroundColor: Color.corbeau,
  },
  divider: {
    height: 1,
    backgroundColor: Color.fadeGreen,
    width: '90%',
    marginVertical: 10,
  },
  ipInputCard: {
    height: Size.consistentPoint * 6,
    width: '80%',
    // backgroundColor: 'grey',
    borderRadius: Size.edge,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputStyle: {
    width: '90%',
    height: Size.consistentPoint * 4,
  },
  inputContainer: {
    backgroundColor: '#cae7ee',
  },
});
