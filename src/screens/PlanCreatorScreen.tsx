import React, {useCallback, useState, useRef, useEffect} from 'react';
import {View, Text, StyleSheet, Animated} from 'react-native';
import {Color, FontSize, Size} from '../utils/CommonStyles';
import {Svg, Circle, Path, G} from 'react-native-svg';
import {
  RouteProp,
  useFocusEffect,
  useNavigation,
  useRoute,
} from '@react-navigation/native';
import {Button, Icon} from 'react-native-elements';
import {SchedulePlan} from '../Types';
import {uuidv4} from '../utils/Utilities';
import {targetPath} from '../utils/CommonInfo';
import {
  HomeStackNavigationProp,
  HomeStackParamList,
} from '../navigators/MainStack';
import {PlanCard} from '../components/PlanCard';
import {HostManager} from '../utils/HostManager';

type PlanCreatorRouteProp = RouteProp<HomeStackParamList, 'PlanCreator'>;

const dummyPlan: SchedulePlan = {
  name: 'New Plan',
  object_type: 'schedule_plan',
  plan_id: 'HJSHDSDIDSHKDS',
  events: [
    {
      event_type: 'output_event',
      relative_timestamp: 0,
      output_amount: [0],
      output_units: ['ml'],
    },
  ],
  comment: 'This is a dummy plan',
};
const hostManager = HostManager.getInstance();

export const PlanCreatorScreen = () => {
  const nav = useNavigation<HomeStackNavigationProp>();
  const route = useRoute<PlanCreatorRouteProp>();
  const hostIP = hostManager.getFormattedHostIP();

  // const generatePlan = () => {
  //   const newPlan: SchedulePlan = {
  //     uuid4: uuidv4(),
  //     name: "Neil's plan - " + Date.now(),
  //     events: [
  //       {
  //         timestamp: Date.now(),
  //         io_state: [1, 0, 1, 0],
  //         io_duration: [10, 0, 20, 0],
  //       },
  //       {
  //         timestamp: Date.now() + 10000,
  //         io_state: [0, 0, 0, 0],
  //         io_duration: [10, 0, 20, 0],
  //       },
  //     ],
  //     object_type: 'schedule_plan',
  //     comment: 'Random comment',
  //   };
  //   return JSON.stringify(newPlan);
  // };

  // const postPlan = async () => {
  //   console.log('POST PLAN');
  //   const path = targetPath + '/uploadplan';
  //   const result = await fetch(path, {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: generatePlan(),
  //   });
  //   const text = await result.text();
  //   console.debug('UPLOAD PLANS RESULT - ', text);
  //   // console.debug('TEXT - ', text);
  //   // const result = await fetch(path, {method: 'POST', body: 'name=Neil'});
  // };

  const handleSendPlan = async (planToSend: SchedulePlan) => {
    console.log('SENDING PLAN - ', planToSend);
    const path = hostIP + '/uploadplan';
    const result = await fetch(path, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(planToSend),
    });
    const text = await result.text();
    console.debug('UPLOAD PLANS RESULT - ', text);
    nav.goBack();
  };

  return (
    <View style={styles.ScreenContainer}>
      <View style={{flex: 1}}>
        <Text style={styles.subHeading}> {'Create a Plan for'} </Text>
        <Text style={styles.text}> {route.params.name} </Text>
      </View>
      <View />
      <View
        style={{
          flex: 3,
          // backgroundColor: 'blue',
          width: '100%',
          alignItems: 'center',
        }}>
        <PlanCard
          planInfo={dummyPlan}
          onPlanConfirmed={(planToSend: SchedulePlan) =>
            handleSendPlan(planToSend)
          }
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  ScreenContainer: {
    flex: 1,
    backgroundColor: Color.corbeau,
    // padding: Size.edge,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollview: {
    width: '100%',
    backgroundColor: Color.pWinterGreen,
    borderRadius: 20,
  },
  newPlanButton: {
    height: 70,
    backgroundColor: Color.mistLightBlue,
    padding: 20,
  },
  text: {
    // flex: 1,
    // marginVertical: 10,
    color: Color.midoriGreen,
    fontSize: FontSize.large * 1.25,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  subHeading: {
    // flex: 1,
    marginVertical: 20,
    color: Color.mistLightGrey,
    fontSize: FontSize.large * 0.8,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  planText: {
    color: Color.indigo,
    fontSize: 20,
    textAlignVertical: 'center',
  },
  svgArea: {
    flex: 2,
    backgroundColor: Color.darkTeal,
    alignItems: 'center',
    justifyContent: 'center',
  },
  planCard: {
    backgroundColor: Color.mistLightBlue,
    height: 80,
    width: '90%',
    alignSelf: 'center',
    marginVertical: 10,
    alignContent: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 10,
  },
});
