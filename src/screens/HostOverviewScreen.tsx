import React, {useCallback, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Color, FontSize, Size} from '../utils/CommonStyles';
import {
  RouteProp,
  useFocusEffect,
  useNavigation,
  useRoute,
} from '@react-navigation/native';
import {Button, Icon} from 'react-native-elements';
import {HostFrontEndResponseEvent} from '../Types';
import {targetPath} from '../utils/CommonInfo';
import {
  HomeStackNavigationProp,
  HomeStackParamList,
} from '../navigators/MainStack';
import LottieView from 'lottie-react-native';
import {ScrollView} from 'react-native-gesture-handler';

type HostOverviewRouteProp = RouteProp<HomeStackParamList, 'HostOverview'>;

export const HostOverviewScreen = () => {
  const nav = useNavigation<HomeStackNavigationProp>();
  const route = useRoute<HostOverviewRouteProp>();
  // useFocusEffect(
  //   useCallback(() => {
  //     asyncFunctions();
  //     // console.debug('HOST OVERVIEW SCREEN');
  //     // getPlans();
  //     // getDevices();
  //   }, []),
  // );

  // const asyncFunctions = async () => {
  //   await getPlans();
  //   await getDevices();
  // };

  // const postPlan = async () => {
  //   console.log('POST PLAN');
  //   const path = targetPath + '/uploadplan';
  //   const result = await fetch(path, {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: generatePlan(),
  //   });
  //   const text = await result.text();
  //   console.debug('UPLOAD PLANS RESULT - ', text);
  //   // console.debug('TEXT - ', text);
  //   // const result = await fetch(path, {method: 'POST', body: 'name=Neil'});
  // };
  // const getPlans = async () => {
  //   const path = targetPath + '/getplans';
  //   const result: Response = await fetch(path, {method: 'GET'});
  //   const text = await result.text();
  //   // const json = await result.json();
  //   // console.debug('GOT PLANS RESULT - ', text);
  //   const object = JSON.parse(
  //     text.replace(/'/g, '"'),
  //   ) as HostFrontEndResponseEvent;
  //   // console.debug('GOT PLANS JSON RESULT - ', object);
  //   console.debug(
  //     'GOT PLANS JSON .data - ',
  //     object.data,
  //     'Length ? ',
  //     object.data.length,
  //   );
  //   const parsedPlans = JSON.parse(object.data) as string[];
  //   console.debug(
  //     ' PARSED PLANS STRING ARRAY = ',
  //     parsedPlans,
  //     'Length - ',
  //     parsedPlans.length,
  //   );
  //   // console.debug("PLANS JSON OBJECT 'event-type' - ", object.event_type);
  //   // console.debug("PLANS JSON OBJECT 'response' - ", object.response);
  //   // console.debug('Plans number - ', object.plans.length);
  //   setPlans(parsedPlans);
  // };
  // const getDevices = async () => {
  //   const path = targetPath + '/getdevices';
  //   const result: Response = await fetch(path, {method: 'GET'});
  //   const text = await result.text();
  //   // const json = await result.json();
  //   // console.debug('GOT DEVICES RESULT - ', text);
  //   const object = JSON.parse(
  //     text.replace(/'/g, '"'),
  //   ) as HostFrontEndResponseEvent;
  //   // console.debug("Device JSON OBJECT 'event-type' - ", object.event_type);
  //   console.debug(
  //     'Device JSON OBJECT .data - ',
  //     object.data,
  //     'Length ? ',
  //     object.data.length,
  //   );

  //   const parsedDevices = JSON.parse(object.data) as string[];
  //   console.debug(
  //     ' PARSED STRING ARRAY = ',
  //     parsedDevices,
  //     'Length - ',
  //     parsedDevices.length,
  //   );
  // };

  const navigateToPlans = () => {
    nav.navigate('PlanScreen', {name: route.params.name});
  };

  return (
    <View style={styles.ScreenContainer}>
      {/* <View style={{flex: 1}}> */}
      <Text style={styles.text}> {route.params.name} </Text>
      {/* </View> */}
      <View
        style={{
          flex: 2,
          width: '90%',
          alignItems: 'center',
        }}>
        <LottieView
          source={require('../assets/lottieAnims/beaker-liquid.json')}
          // source={require('../assets/lottieAnims/Logo_w_Text.json')}
          autoPlay
          loop
        />
        <LottieView
          source={require('../assets/lottieAnims/leaf-sprout.json')}
          // source={require('../assets/lottieAnims/Logo_w_Text.json')}
          autoPlay
          speed={0.01}
          loop
        />
        <Button
          buttonStyle={styles.newPlanButton}
          containerStyle={styles.buttonContainer}
          title={'Plans > '}
          titleStyle={{
            color: Color.corbeau,
            fontSize: FontSize.large * 0.8,
            fontWeight: 'bold',
          }}
          TouchableComponent={TouchableOpacity}
          type={'clear'}
          // iconRight={true}
          // iconContainerStyle={}
          // icon={
          //   <Icon name="chevron-right" type="entypo" color={Color.darkInk} />
          // }
          // onPress={postPlan}
          onPress={navigateToPlans}
        />
      </View>
      {/* <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button
          buttonStyle={styles.newPlanButton}
          titleStyle={{color: Color.indigo}}
          type={'clear'}
          icon={<Icon name="add" type="ionicon" color={Color.darkInk} />}
          // onPress={postPlan}
          onPress={navigateToCreatePlan}
        />
      </View> */}

      {/* <Button title={'POST'} onPress={postData} /> */}
      {/* <Button
        containerStyle={{marginVertical: 20}}
        title={'GET PLANS'}
        onPress={getPlans}
      />
      <Button
        containerStyle={{marginVertical: 20}}
        title={'Upload Plan'}
        onPress={postPlan}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  ScreenContainer: {
    flex: 1,
    backgroundColor: Color.corbeau,
    // padding: Size.edge,
    alignItems: 'center',
    // alignContent: 'center',
    justifyContent: 'center',
  },
  scrollview: {
    width: '100%',
    // backgroundColor: Color.pWinterGreen,
    // borderRadius: 20,
  },
  newPlanButton: {
    // height: Size.consistentPoint * 7,
    // width: '90%',
    borderRadius: Size.consistentPoint * 3.5,
    backgroundColor: Color.midoriGreen,
    padding: 20,
  },
  buttonContainer: {
    position: 'absolute',
    bottom: Size.edge,
    alignSelf: 'center',
    // right: Size.edge,
    height: Size.consistentPoint * 6,
    width: '90%',
    borderRadius: Size.edge * 2,
    // elevation: ,
  },
  text: {
    // flex: 1,
    marginVertical: 20,
    color: Color.mistLightBlue,
    fontSize: FontSize.screenTitle,
    textAlignVertical: 'center',
  },
  subHeading: {
    // flex: 1,
    marginVertical: 20,
    color: Color.pWinterGreen,
    fontSize: 20,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  planText: {
    color: Color.indigo,
    fontSize: FontSize.default,
    textAlignVertical: 'center',
  },
  svgArea: {
    flex: 2,
    backgroundColor: Color.darkTeal,
    alignItems: 'center',
    justifyContent: 'center',
  },
  planCard: {
    backgroundColor: Color.mistLightBlue,
    height: Size.consistentPoint * 6,
    width: '100%',
    alignSelf: 'center',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 10,
  },
});
