import React, {useCallback, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import {Color, FontSize, Size} from '../utils/CommonStyles';
import {
  RouteProp,
  useFocusEffect,
  useNavigation,
  useRoute,
} from '@react-navigation/native';
import {Button, Icon} from 'react-native-elements';
import {HostFrontEndResponseEvent, PlanRunConfiguration} from '../Types';
import {targetPath} from '../utils/CommonInfo';
import {
  HomeStackNavigationProp,
  HomeStackParamList,
} from '../navigators/MainStack';
import {ScrollView} from 'react-native-gesture-handler';
import {HostManager} from '../utils/HostManager';

type PlanScreenRouteProp = RouteProp<HomeStackParamList, 'PlanScreen'>;
const hostManager = HostManager.getInstance();

export const PlanScreen = () => {
  const nav = useNavigation<HomeStackNavigationProp>();
  const route = useRoute<PlanScreenRouteProp>();
  const [planIds, setPlanIds] = useState([] as string[]);
  const hostIP = hostManager.getFormattedHostIP();
  useFocusEffect(
    useCallback(() => {
      asyncFunctions();
      // console.debug('HOST OVERVIEW SCREEN');
      // getPlans();
      // getDevices();
    }, []),
  );

  const asyncFunctions = async () => {
    await getPlans();
    await getDevices();
  };

  const handlePressPlan = (planID: string) =>
    Alert.alert(
      'Start Plan?',
      `Do you want to start the plan with the ID - ${planID}?`,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => handleStartPressedPlan(planID)},
      ],
    );

  const handleStartPressedPlan = async (planIDToStart: string) => {
    console.log(`Starting plan with ID: ${planIDToStart}`);
    const path = hostIP + '/startplan';
    const planConfigToSend: PlanRunConfiguration = {
      object_type: 'plan_run_configuration',
      configuration: {
        start_time: 5,
        plan_id: planIDToStart,
        devices: ['dyrk_DEVICE_0'], // COPIED FROM fakemobile.py
      },
    };
    const result = await fetch(path, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(planConfigToSend),
    });
    const text = await result.text();
    console.debug('START PLANS RESULT - ', text);
  };

  const getPlans = async () => {
    const path = hostIP + '/getplans';
    const result: Response = await fetch(path, {method: 'GET'});
    const text = await result.text();
    // const json = await result.json();
    // console.debug('GOT PLANS RESULT - ', text);
    const object = JSON.parse(
      text.replace(/'/g, '"'),
    ) as HostFrontEndResponseEvent;
    // console.debug('GOT PLANS JSON RESULT - ', object);
    console.debug(
      'GOT PLANS JSON .data - ',
      object.data,
      'Length ? ',
      object.data.length,
    );
    const parsedPlanIds = JSON.parse(object.data) as string[];
    console.debug(
      ' PARSED PLANS STRING ARRAY = ',
      parsedPlanIds,
      'Length - ',
      parsedPlanIds.length,
    );
    // console.debug("PLANS JSON OBJECT 'event-type' - ", object.event_type);
    // console.debug("PLANS JSON OBJECT 'response' - ", object.response);
    // console.debug('Plans number - ', object.plans.length);
    setPlanIds(parsedPlanIds);
  };
  const getDevices = async () => {
    const path = hostIP + '/getdevices';
    const result: Response = await fetch(path, {method: 'GET'});
    const text = await result.text();
    // const json = await result.json();
    // console.debug('GOT DEVICES RESULT - ', text);
    const object = JSON.parse(
      text.replace(/'/g, '"'),
    ) as HostFrontEndResponseEvent;
    // console.debug("Device JSON OBJECT 'event-type' - ", object.event_type);
    console.debug(
      'Device JSON OBJECT .data - ',
      object.data,
      'Length ? ',
      object.data.length,
    );

    const parsedDevices = JSON.parse(object.data) as string[];
    console.debug(
      ' PARSED STRING ARRAY = ',
      parsedDevices,
      'Length - ',
      parsedDevices.length,
    );
  };

  const navigateToCreatePlan = () => {
    nav.navigate('PlanCreator', {name: route.params.name});
  };

  return (
    <View style={styles.ScreenContainer}>
      <View style={{flex: 1}}>
        <Text style={styles.text}> {route.params.name} </Text>
      </View>
      <View style={{flex: 2, width: '90%'}}>
        <Text style={styles.subHeading}> {'Current Plans'} </Text>
        <ScrollView
          style={styles.scrollview}
          contentContainerStyle={{width: '100%'}}>
          {planIds.map((plan, index) => {
            return (
              <TouchableOpacity
                key={plan}
                style={styles.planCard}
                onPress={() => handlePressPlan(plan)}>
                <Text style={styles.planText} key={index}>
                  {plan}
                </Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
        <Button
          buttonStyle={styles.newPlanButton}
          containerStyle={styles.buttonContainer}
          titleStyle={{color: Color.indigo}}
          TouchableComponent={TouchableOpacity}
          type={'clear'}
          icon={<Icon name="add" type="ionicon" color={Color.darkInk} />}
          // onPress={postPlan}
          onPress={navigateToCreatePlan}
        />
      </View>
      {/* <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button
          buttonStyle={styles.newPlanButton}
          titleStyle={{color: Color.indigo}}
          type={'clear'}
          icon={<Icon name="add" type="ionicon" color={Color.darkInk} />}
          // onPress={postPlan}
          onPress={navigateToCreatePlan}
        />
      </View> */}

      {/* <Button title={'POST'} onPress={postData} /> */}
      {/* <Button
        containerStyle={{marginVertical: 20}}
        title={'GET PLANS'}
        onPress={getPlans}
      />
      <Button
        containerStyle={{marginVertical: 20}}
        title={'Upload Plan'}
        onPress={postPlan}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  ScreenContainer: {
    flex: 1,
    backgroundColor: Color.corbeau,
    // padding: Size.edge,
    alignItems: 'center',
    // alignContent: 'center',
    justifyContent: 'center',
  },
  scrollview: {
    width: '100%',
    // backgroundColor: Color.pWinterGreen,
    // borderRadius: 20,
  },
  newPlanButton: {
    height: Size.consistentPoint * 7,
    width: Size.consistentPoint * 7,
    borderRadius: Size.consistentPoint * 3.5,
    backgroundColor: Color.midoriGreen,
    padding: 20,
  },
  buttonContainer: {
    position: 'absolute',
    bottom: Size.edge,
    right: Size.edge,
    height: Size.consistentPoint * 7,
    width: Size.consistentPoint * 7,
    borderRadius: Size.consistentPoint * 3.5,
    elevation: 7,
  },
  text: {
    // flex: 1,
    marginVertical: 20,
    color: Color.mistLightBlue,
    fontSize: FontSize.screenTitle,
    textAlignVertical: 'center',
  },
  subHeading: {
    // flex: 1,
    marginVertical: 20,
    color: Color.pWinterGreen,
    fontSize: 20,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  planText: {
    color: Color.indigo,
    fontSize: FontSize.default,
    textAlignVertical: 'center',
  },
  svgArea: {
    flex: 2,
    backgroundColor: Color.darkTeal,
    alignItems: 'center',
    justifyContent: 'center',
  },
  planCard: {
    backgroundColor: Color.mistLightBlue,
    height: Size.consistentPoint * 6,
    width: '100%',
    alignSelf: 'center',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 10,
  },
});
