import React, {useCallback, useState, useRef, useEffect} from 'react';
import {View, Text, StyleSheet, Animated} from 'react-native';
import {Color, Size} from '../utils/CommonStyles';
import {Svg, Circle, Path, G} from 'react-native-svg';
import {useFocusEffect} from '@react-navigation/native';
import {Button} from 'react-native-elements';
import {SchedulePlan} from '../Types';
import {uuidv4} from '../utils/Utilities';
import {CBPE_IP_FULL_PATH} from '../utils/CommonInfo';

export const DyrkDashboardScreen = () => {
  const [requestIntervalId, setRequestIntervalId] = useState(
    undefined as number | undefined,
  );
  const scaleAnim = useRef(new Animated.Value(25)).current; // Initial value for scale: 1
  const [lxVal, setLxVal] = useState(0);

  useFocusEffect(
    useCallback(() => {
      console.debug('Data screen usefocus callback');
      // fetchData();
      // return () => {
      //   if (requestIntervalId != undefined) {
      //     clearInterval(requestIntervalId);
      //   }
      // };
    }, []),
  );

  const generatePlan = () => {
    const newPlan: SchedulePlan = {
      uuid4: uuidv4(),
      name: "Neil's plan - " + Date.now(),
      events: [
        {timestamp: Date.now(), io_state: [1, 0, 1, 0]},
        {timestamp: Date.now() + 10000, io_state: [0, 0, 0, 0]},
      ],
      object_type: 'schedule_plan',
      comment: 'Random comment',
    };
    return JSON.stringify(newPlan);
  };

  const postData = async () => {
    console.log('POST DATA');
    const path = CBPE_IP_FULL_PATH + '/posttest';
    const result = await fetch(path, {
      method: 'POST',
      body: '{"minute-interval": "5"}',
    });
    // const result = await fetch(path, {method: 'POST', body: 'name=Neil'});
  };

  const postPlan = async () => {
    console.log('POST PLAN');
    const path = CBPE_IP_FULL_PATH + '/uploadplan';
    const result = await fetch(path, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: generatePlan(),
    });
    const text = await result.text();
    console.debug('UPLOAD PLANS RESULT - ', text);
    // console.debug('TEXT - ', text);
    // const result = await fetch(path, {method: 'POST', body: 'name=Neil'});
  };
  //   const result = await fetch(path, {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: `{
  //       "object_type": "schedule_plan",
  //       "name": "Neil's plan",
  //       "uuid4": "7e74b7872a824b4080e687296b84b9dc",
  //       "events": [
  //           {
  //               "timestamp": 1630090295,
  //               "io_state" : [0, 1, 1, 0]
  //           },
  //           {
  //               "timestamp": 1630090297,
  //               "io_state" : [0, 1, 0, 0]
  //           },
  //           {
  //               "timestamp": 1630090299,
  //               "io_state" : [0, 1, 1, 0]
  //           }
  //       ],
  //       "comment": "Blah de Blah"
  //   }`,
  //   });
  //   console.debug('UPLOAD PLANS RESULT - ', result);
  //   const text = await result.text();
  //   console.debug('TEXT - ', text);
  //   // const result = await fetch(path, {method: 'POST', body: 'name=Neil'});
  // };
  const getPlans = async () => {
    const path = CBPE_IP_FULL_PATH + '/getplans';
    const result: Response = await fetch(path, {method: 'GET'});
    const text = await result.text();
    console.debug('GOT PLANS RESULT - ', text);
    // console.debug('TEXT - ', text);

    // const jsonresult = await result.json();
    // console.debug(jsonresult);

    // const result = await fetch(path, {method: 'POST', body: 'name=Neil'});
  };

  const fetchData = async () => {
    const intervalID = setInterval(async () => {
      const path = CBPE_IP + '/light';
      const result = await fetch(path, {method: 'GET'});
      const jsonresult = await result.json();
      console.debug(jsonresult);

      const dataKeys = Object.keys(jsonresult);

      dataKeys.forEach(key => {
        console.debug('KEY - ', key, ' Value = ', jsonresult[key]);
        if (key === 'lightLx') {
          console.debug('BOOM');
          const lightReading = jsonresult[key];
          setLxVal(lightReading > 80 ? 80 : lightReading);
        }
      });
    }, 1000);
    // const intervalID = setInterval(async () => {
    //   console.debug('Trying to fetch data');
    //   const path = VALBY_IP + '/json';
    //   const result = await fetch(path, {method: 'GET'});
    //   const jsonresult = await result.json();
    //   console.debug(jsonresult);

    //   const dataKeys = Object.keys(jsonresult);

    //   dataKeys.forEach(key => {
    //     console.debug('KEY - ', key, ' Value = ', jsonresult[key]);
    //     if (key === 'lightLx') {
    //       console.debug('BOOM');
    //       setLxVal(jsonresult[key]);
    //     }
    //   });
    // }, 1000);
    // setRequestIntervalId(intervalID);
  };

  return (
    <View style={styles.ScreenContainer}>
      {/* <Text style={styles.text}> Light Sensor </Text>
      <Text style={styles.text}> {lxVal + ' lux'} </Text> */}
      {/* <Button title={'POST'} onPress={postData} /> */}
      <Button
        containerStyle={{marginVertical: 20}}
        title={'GET PLANS'}
        onPress={getPlans}
      />
      <Button
        containerStyle={{marginVertical: 20}}
        title={'Upload Plan'}
        onPress={postPlan}
      />
      {/* <View style={styles.svgArea}>
                <Svg style={{width: "100%", height: "100%"}} viewBox="0 0 100 100">
                    <Circle cx="50" cy="50" r="30" fill="yellow" />
                </Svg>
            </View> */}
      {/* <Svg style={{height: '60%', width: '100%'}} viewBox="0 0 100 100">
        <G id="prefix__Leaf">
          <AnimatedCircle
            cx="50"
            cy="50"
            r="30"
            fill={Color.warmSun}
            origin={'50,50'}
            // scale={scaleAnim.interpolate({
            //     inputRange: [0.5, 50.5],
            //     outputRange: [0.5, 1.5],
            // })}
            scale={lxVal / 70 + 0.5}
          />
        </G>
      </Svg> */}
    </View>
  );
};

const styles = StyleSheet.create({
  ScreenContainer: {
    flex: 1,
    backgroundColor: Color.fadeGreen,
    // padding: Size.edge,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    flex: 1,
    color: Color.mistLightBlue,
    fontSize: 26,
    textAlignVertical: 'center',
  },
  svgArea: {
    flex: 2,
    backgroundColor: Color.darkTeal,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
