import React, {useCallback, useState, useRef, useEffect} from 'react';
import {View, Text, StyleSheet, Animated} from 'react-native';
import {Color, FontSize, Size} from '../utils/CommonStyles';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';
import {SchedulePlan} from '../Types';
import {
  HomeStackNavigationProp,
  HomeStackParamList,
} from '../navigators/MainStack';
import {HostManager} from '../utils/HostManager';
import {PlanCreator} from '../components/PlanCreator';

type PlanCreatorRouteProp = RouteProp<HomeStackParamList, 'PlanCreator'>;

const dummyPlan: SchedulePlan = {
  name: 'New Plan',
  object_type: 'schedule_plan',
  plan_id: 'HJSHDSDIDSHKDS',
  events: [
    {
      event_type: 'output_event',
      relative_timestamp: 0,
      output_amount: [0],
      output_units: ['ml'],
    },
  ],
  comment: 'This is a dummy plan',
};
const hostManager = HostManager.getInstance();

export const AddPlanScreen = () => {
  const nav = useNavigation<HomeStackNavigationProp>();
  const route = useRoute<PlanCreatorRouteProp>();
  const hostIP = hostManager.getFormattedHostIP();

  const handleSendPlan = async (planToSend: SchedulePlan) => {
    console.log('SENDING PLAN - ', planToSend);
    const path = hostIP + '/uploadplan';
    const result = await fetch(path, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(planToSend),
    });
    const text = await result.text();
    console.debug('UPLOAD PLANS RESULT - ', text);
    nav.goBack();
  };

  return (
    <View style={styles.screenContainer}>
      <View style={{flex: 1}}>
        <Text style={styles.subHeading}> {'Create a Plan for'} </Text>
        <Text style={styles.text}> {route.params.name} </Text>
      </View>
      <View />
      <View style={styles.mainArea}>
        <PlanCreator
          planInfo={dummyPlan}
          onPlanConfirmed={(planToSend: SchedulePlan) =>
            handleSendPlan(planToSend)
          }
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    // backgroundColor: Color.corbeau,
    backgroundColor: Color.mistLightBlue,
    // padding: Size.edge,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainArea: {
    flex: 5,
    // backgroundColor: 'blue',
    width: '100%',
    alignItems: 'center',
  },
  scrollview: {
    width: '100%',
    backgroundColor: Color.pWinterGreen,
    borderRadius: 20,
  },
  newPlanButton: {
    height: 70,
    backgroundColor: Color.mistLightBlue,
    padding: 20,
  },
  text: {
    // flex: 1,
    // marginVertical: 10,
    color: Color.midoriGreen,
    fontSize: FontSize.large * 1.25,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  subHeading: {
    // flex: 1,
    marginVertical: 20,
    color: Color.corbeau,
    fontSize: FontSize.large * 0.8,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  planText: {
    color: Color.indigo,
    fontSize: 20,
    textAlignVertical: 'center',
  },
  svgArea: {
    flex: 2,
    backgroundColor: Color.darkTeal,
    alignItems: 'center',
    justifyContent: 'center',
  },
  planCard: {
    backgroundColor: Color.mistLightBlue,
    height: 80,
    width: '90%',
    alignSelf: 'center',
    marginVertical: 10,
    alignContent: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 10,
  },
});
