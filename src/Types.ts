// export interface SchedulePlan {
//   object_type: string;
//   name: string;
//   uuid4: string;
//   events: OutputEvent[];
//   comment: string;
// }
export interface SchedulePlan {
  object_type: string;
  name: string;
  /** A UUID for the plan */
  plan_id: string;
  /**
   * An Array of output events
   */
  events: OutputEvent[];
  comment: string;
}

export interface PlanRunConfiguration {
  object_type: string;
  configuration: {
    start_time: number;
    plan_id: string;
    devices: string[]; // Array of device ids??
  };
}

// export interface Event {
//   timestamp: number;
//   io_state: number[];
//   io_duration: number[];
// }

export enum TaskType {
  WATER,
  LIGHT,
  //HEAT?
  //HUMIDITY?
}

export type Interval =
  | 'none'
  | 'second'
  | 'minute'
  | 'hour'
  | 'day'
  | 'week'
  | 'month';

export const allIntervals: Interval[] = [
  'none',
  'second',
  'minute',
  'hour',
  'day',
  'week',
  'month',
];
export type TaskTypeName = 'water' | 'light';
export type unitTypes = 'ml' | 'lx';
export interface TaskTypeObject {
  index: number;
  typeName: TaskTypeName;
}

export const TaskTypes: TaskTypeObject[] = [
  {
    index: 0,
    typeName: 'water',
  },
  {
    index: 1,
    typeName: 'light',
  },
];

export interface OutputEvent {
  // Deprecates Event object.
  event_type: string;
  relative_timestamp: number; // Relative to the starttime of the plan.
  output_amount: number[];
  output_units: unitTypes[];
}

// New host response ?
export interface HostFrontEndResponseEvent {
  event_type: string;
  message: string;
  // data: string[] | HostMainControllerResponseEvent | HostFrontEndResponseEvent;
  data: string; // Can parse the string to any data type depending on the host response?}
}
// export interface HostMainControllerResponseEvent {
//   event_type: string;
//   response: HostResponseObject;
// }

export interface HostMainControllerResponseEvent {
  event_type: string;
  response: HostResponseObject;
}
export interface HostResponseObject {
  message: string;
  // data: string[] | HostMainControllerResponseEvent | HostFrontEndResponseEvent;
  data: string; // Can parse the string to any data type depending on the host response?
}
