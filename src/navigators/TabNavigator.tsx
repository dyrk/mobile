import {
  createBottomTabNavigator,
  BottomTabNavigationProp,
} from '@react-navigation/bottom-tabs';
import * as React from 'react';
import {HomeScreen} from '../screens/HomeScreen';
import {DyrkDashboardScreen} from '../screens/DyrkDashboardScreen';
import {Color} from '../utils/CommonStyles';

export type TabNavigatorParamList = {
  Home: undefined;
  Dashboard: undefined;
};

export type TabNavigatorProp = BottomTabNavigationProp<TabNavigatorParamList>;
const Tab = createBottomTabNavigator<TabNavigatorParamList>();

export const TabNavigator: React.FC<{}> = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {backgroundColor: Color.pWinterGreen},
        tabBarLabelStyle: {fontSize: 30, textAlignVertical: 'center'},
        tabBarActiveTintColor: Color.indigo,
        tabBarInactiveTintColor: Color.mistLightGrey,
      }}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Dashboard" component={DyrkDashboardScreen} />
    </Tab.Navigator>
  );
};
