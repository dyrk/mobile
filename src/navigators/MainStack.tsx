import {
  CardStyleInterpolators,
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import * as React from 'react';
import {AddPlanScreen} from '../screens/AddPlanScreen';
import {HomeScreen} from '../screens/HomeScreen';
import {HostOverviewScreen} from '../screens/HostOverviewScreen';
import {PlanCreatorScreen} from '../screens/PlanCreatorScreen';
import {PlanScreen} from '../screens/PlanScreen';
import {Color} from '../utils/CommonStyles';

export type HomeStackParamList = {
  Home: undefined;
  HostOverview: {name: string}; // should later be the Id of the host etc.
  PlanScreen: {name: string}; // huld later be Id of th host / plant / bio.
  PlanCreator: {name: string}; // should later be the Id of the host etc.
};

export type HomeStackNavigationProp = StackNavigationProp<HomeStackParamList>;

const Stack = createStackNavigator<HomeStackParamList>();

export const MainStack: React.FC = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen
        options={{
          headerShown: true,
          headerTransparent: true,
          headerTitle: '',
          headerTintColor: 'white',
        }}
        name="HostOverview"
        component={HostOverviewScreen}
      />
      <Stack.Screen
        options={{
          headerShown: true,
          headerTransparent: true,
          headerTitle: '',
          headerTintColor: 'white',
        }}
        name="PlanScreen"
        component={PlanScreen}
      />
      <Stack.Screen
        options={{
          headerShown: true,
          headerTransparent: true,
          headerTitle: '',
          headerTintColor: Color.corbeau,
          cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
        }}
        name="PlanCreator"
        component={AddPlanScreen}
      />
    </Stack.Navigator>
  );
};
